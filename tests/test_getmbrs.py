# Test getmbrs_ang.py
#
# Copyright (C) 2017 Francesco Montanari, Ghassem Gozaliasl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import sys
import unittest

from astropy.io import fits
import getmbrs.getmbrs as gm


SELF_PATH = os.path.split(sys.argv[0])[0] # Path to this file


class TestRegressionAngular(unittest.TestCase):
    """This test verifies if the number of selected members and BGGs
    coincides with the one obtained in test cases and verified by
    hand. Here we apply the mask based on angular coordinates.
    """
    def setUp(self):
        # Expected number of selected members
        self.nmembers = 302
        # Expected number of selected BGGs
        self.nbgg = 9

        # Set data and output paths
        self.catcluster = os.path.join(SELF_PATH,
                                       "data/cluster_catalog_angular.fits")
        self.catgal = os.path.join(SELF_PATH,
                                   "data/galaxy_catalog_angular.fits")
        self.outpath = os.path.join(SELF_PATH,
                                   "output/")
        if not os.path.isdir(self.outpath):
            os.mkdir(self.outpath)

    def tearDown(self):
        # Remove output directory
        if os.path.isdir(self.outpath):
            shutil.rmtree(self.outpath)

    def test_getmbrs(self):
        """Test if the number of selected members and BGGs is the
        expected one.
        """
        # Members
        catmembers = gm.getmbrs(self.catcluster, self.catgal,
                                masktype="angular",
                                outpath=self.outpath)
        hdu = fits.open(catmembers)
        table = hdu[1].data
        self.assertEqual(len(table),self.nmembers)

        # BGGs
        catbgg = gm.getbgg(catmembers, nbgg=3, masktype="angular",
                           outpath=self.outpath)
        hdu = fits.open(catbgg)
        table = hdu[1].data
        self.assertEqual(len(table),self.nbgg)


class TestRegressionCartesian(unittest.TestCase):
    """This test verifies if the number of selected members and BGGs
    coincides with the one obtained in test cases and verified by
    hand. Here we apply the mask based on angular coordinates.
    """
    def setUp(self):
        # Expected number of selected members
        self.nmembers = 6
        # Expected number of selected BGGs
        self.nbgg = 3

        # Set data and output paths
        self.catcluster = os.path.join(SELF_PATH,
                                       "data/cluster_catalog_cartesian.fits")
        self.catgal = os.path.join(SELF_PATH,
                                   "data/galaxy_catalog_cartesian.fits")
        self.outpath = os.path.join(SELF_PATH,
                                   "output/")
        if not os.path.isdir(self.outpath):
            os.mkdir(self.outpath)

    def tearDown(self):
        # Remove output directory
        if os.path.isdir(self.outpath):
            shutil.rmtree(self.outpath)

    def test_getmbrs(self):
        """Test if the number of selected members and BGGs is the
        expected one.
        """
        # Members
        catmembers = gm.getmbrs(self.catcluster, self.catgal,
                                masktype="cartesian",
                                outpath=self.outpath)
        hdu = fits.open(catmembers)
        table = hdu[1].data
        self.assertEqual(len(table),self.nmembers)

        # BGGs
        catbgg = gm.getbgg(catmembers, nbgg=3, masktype="cartesian",
                           outpath=self.outpath)
        hdu = fits.open(catbgg)
        table = hdu[1].data
        self.assertEqual(len(table), self.nbgg)


if __name__ == '__main__':
    unittest.main()
