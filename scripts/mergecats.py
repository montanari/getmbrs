# Merge iteratively several FITS catalogs.

import glob
import sys
sys.path.append('./getmbrs')
import fits

#fnames = glob.glob("/home/francesco/data/Millennium/SIM/output/*members.fits");
#FNAMES = glob.glob("./tests/data/*.fits");
ROOT = "/home/francesco/data/Millennium/SIM/output/"
FNAMES = [ROOT+"wmap.014.cluster.members.fits",
          ROOT+"wmap.015.cluster.members.fits"]

OUTTABLE = 'out.fits'

print("Merging:")
fnametot = FNAMES[0]
for fname in FNAMES[1:]:
    print("\t"+fname)
    fits.append_fits(fnametot,fname,OUTTABLE)
    if fnametot == FNAMES[0]:
        fnametot = OUTTABLE

print("Merged catalog: "+fnametot)
