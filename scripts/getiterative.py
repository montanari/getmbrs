"""Dumb script to run getmbrs iteratively for several catalogs."""

import getmbrs as gm
import os

# Format: [[cluster1,gal1],[cluster2,gal2],...]
cats = [["data/wmap.128.cluster.fits","data/wmap.128.galaxies.fits"]]

ncat = len(cats)

for i in range(ncat):
    # Compute members catalog
    icat = cats[i]
    cl   = icat[0]
    gal  = icat[1]
    gm.getmbrs(cl,gal,verbose=False)

    # Extract the two brighter members from each cluster
    bgg = cl.replace(".fits",".members.fits")
    head, tail = os.path.split(bgg)
    bgg = os.path.join("output/",tail)
    gm.getbgg(bgg,nbgg=2,verbose=False)
