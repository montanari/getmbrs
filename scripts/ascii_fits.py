import pyfits
from scipy import *
from pylab import *
import numpy
i = int(input("Enter the source id number:"))
M=numpy.loadtxt("wmap.0"+str(i)+".cluster.cat")

# ihal l b r z_true z_obs Mvir Rvir M500 R500 Mstar500 Mgas500 T500 Lx500 Ysz500 M200 R200

c1=pyfits.Column(name='ihal', format='I', array=M[:,0])
c2=pyfits.Column(name='l', format='D', array=M[:,1])
c3=pyfits.Column(name='b', format='D', array=M[:,2])
c4=pyfits.Column(name='r', format='D', array=M[:,3])
c5=pyfits.Column(name='z_true', format='D', array=M[:,4])
c6=pyfits.Column(name='z_obs', format='D', array=M[:,5])
c7=pyfits.Column(name='Mvir', format='D', array=M[:,6])
c8=pyfits.Column(name='Rvir', format='D', array=M[:,7])
c9=pyfits.Column(name='M500', format='D', array=M[:,8])
c10=pyfits.Column(name='R500', format='D', array=M[:,9])
c11=pyfits.Column(name='Mstar', format='D', array=M[:,10])
c12=pyfits.Column(name='Mgas500', format='D', array=M[:,11])
c13=pyfits.Column(name='T500', format='D', array=M[:,12])
c14=pyfits.Column(name='Lx500', format='D', array=M[:,13])
c15=pyfits.Column(name='Ysz500', format='D', array=M[:,14])
c16=pyfits.Column(name='M200', format='D', array=M[:,15])
c17=pyfits.Column(name='R200', format='D', array=M[:,16])

cols = pyfits.ColDefs([c1, c2, c3, c4, c5, c6, c7, c8, c9,
                       c10,c11,c12,c13,c14,c15,c16,c17])
tbhdu = pyfits.new_table(cols)
hdu = pyfits.PrimaryHDU(data=M)
thdulist = pyfits.HDUList([hdu,tbhdu])
thdulist.writeto("wmap.0"+str(i)+".cluster.fits")
thdulist.close()
# isub l b rr vmax z_true z_obs Mstar sfr u V  g  r  i  z  Y  J  H  K  L  M Age Z'
gal=numpy.loadtxt("wmap.0"+str(i)+".galaxies.cat")
cc1=pyfits.Column(name='isub', format='I', array=gal[:,0])
cc2=pyfits.Column(name='l', format='D', array=gal[:,1])
cc3=pyfits.Column(name='b', format='D', array=gal[:,2])
cc4=pyfits.Column(name='rr', format='D', array=gal[:,3])
cc5=pyfits.Column(name='z_true', format='D', array=gal[:,4])
cc6=pyfits.Column(name='z_obs', format='D', array=gal[:,5])
cc7=pyfits.Column(name='Mstar', format='D', array=gal[:,6])
cc8=pyfits.Column(name='sfr', format='D', array=gal[:,7])
cc9=pyfits.Column(name='u', format='D', array=gal[:,8])
cc10=pyfits.Column(name='V', format='D', array=gal[:,9])
cc11=pyfits.Column(name='g', format='D', array=gal[:,10])
cc12=pyfits.Column(name='r', format='D', array=gal[:,11])
cc13=pyfits.Column(name='i', format='D', array=gal[:,12])
cc14=pyfits.Column(name='z', format='D', array=gal[:,13])
cc15=pyfits.Column(name='Y', format='D', array=gal[:,14])
cc16=pyfits.Column(name='J', format='D', array=gal[:,15])
cc17=pyfits.Column(name='H', format='D', array=gal[:,16])
cc18=pyfits.Column(name='K', format='D', array=gal[:,17])
cc19=pyfits.Column(name='L', format='D', array=gal[:,18])
cc20=pyfits.Column(name='M', format='D', array=gal[:,19])
cc21=pyfits.Column(name='Age', format='D', array=gal[:,20])
cc22=pyfits.Column(name='Z', format='D', array=gal[:,21])


ccols = pyfits.ColDefs([cc1, cc2, cc3, cc4, cc5, cc6, cc7, cc8, cc9,
                        cc10,cc11,cc12,cc13,cc14,cc15,cc16,cc17,cc18,
                        cc19,cc20,cc21])
tbhdu1 = pyfits.new_table(ccols)
hdu1 = pyfits.PrimaryHDU(data=gal)
thdulist1 = pyfits.HDUList([hdu1,tbhdu1])
thdulist1.writeto("wmap.0"+str(i)+".galaxies.fits")
thdulist1.close()
