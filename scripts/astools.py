from astropy.coordinates import SkyCoord
from astropy import units as u
import numpy as np

def get_dist_gal(l1,b1,r1,l2,b2,r2):
    """Compute comoving separation in Mpc given Galactic coordinates of
    two sets of objects. The coordinates can be arrays, and the array
    of the difference will be passed.

    Parameters
    ----------
    l1 : array_like
        Galactic l in degrees, first object.
    b1 : array_like
        Galactic b in degrees, first object.
    r1 : array_like
        Proper distance in Mpc, first object.
    l2 : array_like
        Galactic l in degrees, second object.
    b2 : array_like
        Galactic b in degrees, second object.
    r2 : array_like
        Proper distance in Mpc, second object.

    Returns
    -------
    dist : array_like
        Proper separation between the objects in Mpc.
    """
    # Spherical to cartesian
    c1 = SkyCoord(l=l1*u.deg, b=b1*u.deg,
                  distance=r1*u.Mpc,
                  frame='galactic')
    x1 = c1.cartesian.x
    y1 = c1.cartesian.y
    z1 = c1.cartesian.z

    c2 = SkyCoord(l=l2*u.deg, b=b2*u.deg,
                  distance=r2*u.Mpc,
                  frame='galactic')
    x2 = c2.cartesian.x
    y2 = c2.cartesian.y
    z2 = c2.cartesian.z

    #dist = np.sqrt( (x1-x2)**2 +(y1-y2)**2 +(z1-z2)**2)
    distpy = c1.separation_3d(c2)

    return dist.value




# distance_cl = cluster['r_cl'] # Mpc
# rvir = cluster['R200_cl'] * 1.e-3 / hubble # Mpc
# dA_cl = (1+z)*distance_cl
# theta_vir = rvir/dA

l1 = [80.,81.,82.]
b1 = [10.,11.,12.]
r1 = [100.,110.,120.]
l2 = [83.,84.,85.]
b2 = [13.,14.,15.]
r2 = [130.,140.,150.]

get_dist_gal(l1,b1,r1,l2,b2,r2)
