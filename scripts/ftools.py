# fits - handle fits file
# Copyright (C) 2017 Francesco Montanari, Ghassem Gozaliasl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from astropy.table import Table, hstack
from astropy.io import fits

def append(fname1,fname2,fout):
    """Append fits file by rows."""
    t1 = fits.open(fname1)
    t2 = fits.open(fname2)

    nrows1 = t1[1].data.shape[0]
    nrows2 = t2[1].data.shape[0]
    nrows = nrows1 + nrows2

    hdu = fits.BinTableHDU.from_columns(t1[1].columns, nrows=nrows)
    for colname in t1[1].columns.names:
        hdu.data[colname][nrows1:] = t2[1].data[colname]

    try:
        hdu.writeto(fout,overwrite=True)
    except TypeError:
        hdu.writeto(fout,clobber=True) # Deprecated

    t1.close()
    t2.close()

    return 0
