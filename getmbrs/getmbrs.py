"""getmbrs selects member galaxies given a clusters and a galaxies catalog.

Invoke help message from command line as:
    python getmbrs.py --help

Copyright 2016, 2017 Francesco Montanari, Ghassem Gozaliasl

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import os
import warnings

from astropy.io import fits
import numpy as np

from . import catalogs as cat

def getmbrs(catcluster, catgal, masktype="cartesian", outpath="output/",
            verbose=False):
    """Select member galaxies given a clusters and a galaxies catalog.
    For each cluster, determine which galaxies are members by
    establishing if the galaxy coordinates are within the cluster
    virial radius.  Produce a combined catalog listing all the member
    galaxies.  For each member galaxy line, the information about the
    relative cluster is also attached.  It adds a column 'bgg_rank'
    ranging from zero to the number of members of a given cluster,
    according to the r band luminosity (brightest correspond to zero
    index).

    Parameters
    ----------
    catcluster : str
        Path to clusters catalog (FITS table).
    catgal : str
        Path to galaxies catalog (FITS table).
    outpath : str
        Path to the output directory. It must already exist.
    verbose : bool (default: False)
        If True, return verbose output.

    Return
    ------
    fitsout : str
        Path of the file containing the combined FITS table. The file
        has the same root as catcluster, with an additional '.members'
        label.
    """

    fitsout = catcluster.replace(".fits", ".members.fits")
    head, tail = os.path.split(fitsout)
    fitsout = os.path.join(outpath, tail)

    if verbose is True:
        print("Reading catalogs and setting coordinates.")

    if masktype == "cartesian":
        cluster = cat.ClusterCartesian(catcluster)
        galaxy = cat.GalaxyCartesian(catgal)
    elif masktype == "angular":
        cluster = cat.ClusterAngular(catcluster)
        galaxy = cat.GalaxyAngular(catgal)
    else:
        raise NotImplementedError("Catalog not implemented.")

    ncl = len(cluster.table)
    ngal = len(galaxy.table)
    bggrank = np.array([])        # BGG rank.
    dist_to_cl = np.array([]) # Members distance to cluster centers.
    init = False

    if verbose is True:
        print("Identifying members of cluster.")

    for i in range(ncl):
        if verbose is True:
            print("\t" + cluster.ihal_key + " = " +
                  str(cluster.table[cluster.ihal_key][i]))

        mask_cl = np.asarray([False]*ncl)
        mask_cl[i] = True
        ihost = cluster.table[mask_cl]

        # Match galaxies to the host cluster
        mask_gal = cluster.mask_gal(i, galaxy)

        # Degeneracy: how many members per cluster
        deg = len(galaxy.table[mask_gal])

        galmasked = galaxy.table[mask_gal]

        if len(galmasked) > 0:
            # Proceed if a member exists.

            # BGG rank.
            mbrs_mass = galaxy.mass[mask_gal] # members r band
            nmbrs = len(mbrs_mass)
            sortid = np.argsort(-mbrs_mass) # indices that would sort array
            sortrank = np.empty(nmbrs, int)
            sortrank[sortid] = np.arange(nmbrs) # assign rank to sorted array
            bggrank = np.append(bggrank, sortrank)

            # Members distances to cluster center.
            mbrs_dist_to_cl = cluster.get_distance(i, galaxy)[mask_gal]
            dist_to_cl = np.append(dist_to_cl, mbrs_dist_to_cl)

            if init is False:
                # Initialize the members and hosts catalogs
                members = galmasked
                hosts = cluster.table[mask_cl]
                for k in range(deg-1):
                    hosts = np.append(hosts, ihost, axis=0)
                init = True
            else:
                members = np.append(members, galaxy.table[mask_gal], axis=0)
                for k in range(deg):
                    hosts = np.append(hosts, ihost, axis=0)

    members, hosts, bggrank, dist_to_cl = _rm_dupes(galaxy.galid_name,
                                                    members, hosts,
                                                    bggrank,
                                                    dist_to_cl)
    _writembrs(members, hosts, bggrank, init, fitsout, verbose)

    return fitsout


def _get_repeated_idx(nparray):
    """Get a list of all indices of repeated elements in a numpy array."""
    idx_sort = np.argsort(nparray)
    sorted_records_array = nparray[idx_sort]
    vals, idx_start, count = np.unique(sorted_records_array,
                                       return_counts=True,
                                       return_index=True)

    # Sets of indices.
    res = np.split(idx_sort, idx_start[1:])

    # Filter them with respect to their size, keeping only items
    # occurring more than once.
    vals = vals[count > 1]
    res = filter(lambda x: x.size > 1, res)

    return res


def _rm_dupes(galid_name, members, hosts, bggrank, dist_to_cl):
    """Remove duplicates: a galaxy can be a member only of one cluster. In
    case of clusters overlap, a galaxy may be selected as a member of
    them all. Select it as a member only of the closer cluster.
    """
    # Determine the column index for galid.
    hdutmp = fits.BinTableHDU(data=members)
    index_galid, = np.where(np.array(hdutmp.data.columns.names) == galid_name)
    if len(index_galid) > 1:
        msg = "More than one column correspond to {}".format(galid_name)
        raise RuntimeError(msg)
    else:
        index_galid = index_galid[0]

    # Recover all the galid's.
    ids = np.array([elem[index_galid] for elem in members])

    # Deal with duplicates.
    mask = np.ones_like(ids, dtype=np.bool) # Initialize all to True.

    # Elements of dupes_idx are arrays containing indices
    # corresponding to a same galid value.
    dupes_idx = _get_repeated_idx(ids)

    for elems in dupes_idx:
        # Index (within elems) to keep: the one corresponding to a
        # smaller distance to the respective cluster center.
        keep_elems_idx = np.argmin(dist_to_cl[elems])
        # Indices (within ids) to remove.
        rm_ids_idx = elems[np.arange(len(elems))!=keep_elems_idx]
        mask[rm_ids_idx] = False

    return members[mask], hosts[mask], bggrank[mask], dist_to_cl[mask]


def _writembrs(members, hosts, bggrank, init, fitsout,
               verbose=False):
    """Write the merged members and hosts catalog."""

    if init is True:
        if len(members) != len(hosts):
            raise IndexError("Mismatch in derived catalogs.\n"
                             +"Members: %g rows\n" % len(members)
                             +"Hosts: %g rows" % len(hosts))
        elif len(members) != len(bggrank):
            raise IndexError("Mismatch between the BGG rank array and "
                             +"members catalog.\n"
                             +"Members: %g rows\n" % len(members)
                             +"Rank vector: %g rows" % len(bggrank))
    else:
        warnings.warn("No members detected", Warning)

    # Merge members and host catalog (each host repeated over its
    # members).
    hdumem = fits.BinTableHDU(data=members)
    hduhost = fits.BinTableHDU(data=hosts)
    new_columns = hdumem.columns + hduhost.columns
    hdu = fits.BinTableHDU.from_columns(new_columns)

    # Add BGG rank and distances columns.
    orig_table = hdu.data
    orig_cols = orig_table.columns
    new_cols = fits.ColDefs([fits.Column(name='bgg_rank', format='I',
                                         array=bggrank)])
    # new_cols2 = fits.ColDefs([fits.Column(name='distance_to_cl', format='D',
    #                                      array=dist_to_cl)])
    hdux = fits.BinTableHDU.from_columns(orig_cols + new_cols)

    try:
        hdux.writeto(fitsout, overwrite=True)
    except TypeError:
        hdux.writeto(fitsout, clobber=True) # Deprecated

    if verbose is True:
        print("--> "+fitsout)


def getbgg(catmembers, nbgg=1, masktype="cartesian", outpath="output/",
           verbose=False):
    """Extract only the brightest galaxies given a members catalog obained
    with getmbrs(). It first select only those members close enough to the
    brightest galaxy (BGG) of each cluster. Then, it selects the nbgg most
    bright galaxies among the selected members.

    Parameters
    ----------
    catmembers : str
        Path to members catalog (FITS table), formatted consistently with
        the output of getmbrs().
    nbgg : int (default: 1)
        Set how many brighter galaxies to select. If 1, only the BGG is
        selected. If larger, the corresponding number of brighter galaxies
        close enough to BGG (if any) is selected.
    outpath : str
        Path to the output directory. It must already exist.
    verbose : bool (default: False)
        If True, return verbose output.

    Return
    ------
    fitsout : str
        Path to the file containing the combined FITS table. It is
        saved to the ./output/ directory. The file has the same root
        as catcluster, with an additional '.members' label.
    """

    if nbgg < 1:
        raise ValueError("nbgg must be an integer >= 1.")

    fitsout = catmembers.replace(".fits", ".bgg.fits")
    head, tail = os.path.split(fitsout)
    fitsout = os.path.join(outpath, tail)

    if verbose is True:
        print("Reading catalogs and setting coordinates.")

    if masktype == "cartesian":
        member = cat.MemberCartesian(catmembers)
    elif masktype == "angular":
        member = cat.MemberAngular(catmembers)
    else:
        raise NotImplementedError("Catalog not implemented.")

    nmem = len(member.table)
    # Number of brighter galaxies per cluster.
    nbgg0 = len(member.table_bgg)

    if verbose is True:
        print("Selecting BGGs.")

    # Create a new object for the brighter members mask, to avoid
    # modifying the member.mask_bgg mask.
    mask_nbgg = np.array(member.mask_bgg)

    for i in range(nbgg0):
        if verbose is True:
            print("\t" + member.ihal_key + " = " +
                  str(member.table_bgg[member.ihal_key][i]))
        halo = member.ihal_bgg[i]

        # Select galaxies close enough to the BGG.
        submask = member.submask_bgg(i)

        # Among the members close to BGG, select the N brighter ones
        # (but fainter than the BGG).
        submem = member.memrank[submask]
        sortid = np.argsort(submem)
        sortsubmem = submem[sortid]
        selection = sortsubmem[:nbgg]
        for j in range(nmem):
            if (halo == member.ihal_cl[j]) and (member.memrank[j] in selection):
                mask_nbgg[j] = True

    hdux = fits.BinTableHDU(data=member.table[mask_nbgg])
    try:
        hdux.writeto(fitsout, overwrite=True)
    except TypeError:
        hdux.writeto(fitsout, clobber=True) # Deprecated
    if verbose is True:
        print("--> "+fitsout)

    return fitsout


def getrand(cat, nrows, verbose=False):
    """Extract random rows from a FITS catalog (without replacement).

    Parameters
    ----------
    cat: str
        Path to the catalog.
    nrows: int
        Number of randomly selected rows.

    Returns
    -------
    fitsout: str
        Path to the catalog containing the randomly selected rows.
    """

    fitsout = cat.replace(".fits", ".red"+str(nrows)+".fits")
    head, tail = os.path.split(fitsout)
    fitsout = os.path.join("output/", tail)

    hdu = fits.open(cat)
    fulldata = hdu[1].data

    randdata = np.random.choice(fulldata, size=nrows, replace=False)

    hdux = fits.BinTableHDU(data=randdata)
    hdux.writeto(fitsout, clobber=True)
    if verbose is True:
        print("--> "+fitsout)

    hdu.close()

    return fitsout


def parse():
    parser = argparse.ArgumentParser(description='Select member galaxies.')
    parser.add_argument('--version', action='version', version='%(prog)s 0.1')
    parser.add_argument('-v', '--verbose', action='count')

    parser.add_argument('--masktype', default='cartesian',
                        choices=['cartesian', 'angular'],
                        help=("what coordinates to consider when "
                              +"applying the mask (default: cartesian)"))

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-m', '--members',
                       metavar=('clusters.fits', 'galaxies.fits'),
                       type=str, nargs=2,
                       help="select members from cluster and galaxy FITS "
                       +"catalogs")

    group.add_argument('-b', '--bgg', metavar=('N', 'members.fits'),
                       type=str, nargs=2,
                       help="select N brighter galaxies from FITS members "
                       +"catalog")

    group.add_argument('-r', '--rand',
                       metavar=('N', 'catalog.fits'),
                       type=str, nargs=2,
                       help="select random subset of N rows from the catalog")

    args = parser.parse_args()

    if args.verbose is not None:
        vl = True
    else:
        vl = False

    if args.members is not None:
        catcluster = args.members[0]
        catgal = args.members[1]
        getmbrs(catcluster, catgal, masktype=args.masktype, verbose=vl)

    if args.bgg is not None:
        N = int(args.bgg[0])
        catmem = args.bgg[1]
        getbgg(catmem, nbgg=N, masktype=args.masktype, verbose=vl)

    if args.rand is not None:
        N = int(args.rand[0])
        cat = args.rand[1]
        getrand(cat, nrows=N, verbose=vl)


if __name__ == "__main__":
    parse()
