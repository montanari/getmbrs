"""Define objects according to the specific content of a FITS
catalog. The classes defined in this module are supposed to be
modified by the user, according to the specific catalogs.

Copyright 2017 Francesco Montanari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

try:
    from functools import lru_cache
except ImportError:
    from backports.functools_lru_cache import lru_cache

from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.cosmology import WMAP7 as cosmo
from astropy.io import fits
import numpy as np

class ClusterAngular:
    """Define here the clusters FITS table and related useful
    quantities. Table entries must be adapted to the specific
    catalog. Define also a method to match member galaxies. The mask
    is defined based on angular coordinates.

    Parameters
    ----------
    catcluster : str
        Path to clusters catalog (FITS table).
    """

    def __init__(self, catcluster):
        hducluster = fits.open(catcluster)

        # Add label '_cl' to each header, to distinguish clusters from
        # galaxies entries.
        hdrcluster = hducluster[1].header
        for key in hdrcluster.keys():
            kval = hducluster[1].header[key]
            if (isinstance(kval, basestring)) and ('TTYPE' in key):
                hdrcluster[key] = kval + "_cl"

        # FITS data table
        self.table = hducluster[1].data

        # Useful quantities.
        self.ra = self.table['ra_cl']
        self.dec = self.table['dec_cl']
        self.z = self.table['redshift_cl']
        self.rvir = self.table['R200_deg_cl']
        self.ihal_key = 'id_cl'
        self.ihal = self.table[self.ihal_key]

        # Angular coordinates (Astropy object).
        self.coords = SkyCoord(self.ra*u.deg, self.dec*u.deg,
                               frame='fk5')

        hducluster.close()

    @lru_cache()
    def _compute_separation(self, ii, galaxy):
        """Return angular and redshift separations from the center of the
        cluster. Since this method is likely to be called with the
        same arguments by other ones, a memoization decorator is used.

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.
        galaxy : obj
            Instance of the GalaxyAngular class.

        Return
        ------
        ang_sep, z_sep : (array, array)
            Angular separation (Astropy quantity: angle) and redshift
            separation.

        """
        ang_sep = self.coords[ii].separation(galaxy.coords)
        z_sep = np.abs(self.z[ii] - galaxy.z)
        return ang_sep, z_sep

    def get_distance(self, ii, galaxy):
        """Get the distance from the center of the cluster, computed as the
        hypotenuse given by the angular and redshift separation.

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.
        galaxy : obj
            Instance of the GalaxyAngular class.

        Return
        ------
        hypot : array
            Hypotenuse based on the angular and redshift separations.
        """
        ang_sep, z_sep = self._compute_separation(ii, galaxy)
        ang_sep = ang_sep.radian
        return np.hypot(ang_sep, z_sep)

    def mask_gal(self, ii, galaxy):
        """Create a mask by matching galaxies close enough to the
        given cluster.

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.
        galaxy : GalaxyAngular
            Instance of the Galaxy class.

        Return
        ------
        mask_gal : array
            Mask for the galaxy catalog. Array of Boolean elements,
            'True' if the galaxy is a cluster member.
        """
        ang_sep, z_sep = self._compute_separation(ii, galaxy)
        ang_sep = ang_sep.deg

        cond1 = ang_sep < self.rvir[ii]
        cond2 = z_sep < 0.02*(1.+self.z[ii])
        cond3 = galaxy.rband < 0.
        mask_gal = cond1*cond2*cond3 # Remember: '*' = 'AND'

        return mask_gal


class GalaxyAngular:
    """Define here the galaxy FITS table and related useful
    quantities. Table entries must be adapted to the specific
    catalog. We assume that a mask will be applied in terms of angular
    coordinates.

    Parameters
    ----------
    catgal : str
        Path to clusters catalog (FITS table).
    """

    def __init__(self, catgal):
        hdugalaxy = fits.open(catgal)

        # FITS data table
        self.table = hdugalaxy[1].data

        # Useful quantities.
        self.galid_name = 'galid'
        self.galid = self.table[self.galid_name]
        self.ra = self.table['ra']
        self.dec = self.table['dec']
        self.z = self.table['zpdf']
        self.rband = self.table['MR']
        self.mass = self.table['mass_med']

        self.coords = SkyCoord(self.ra*u.deg, self.dec*u.deg,
                               frame='fk5')

        hdugalaxy.close()


class MemberAngular:
    """Define here the members FITS table and related useful
    quantities. Table entries must be adapted to the specific
    catalog. Define also a method to select BGGs. The mask is applied
    based on angular coordinates.

    Parameters
    ----------
    catcluster : str
        Path to clusters catalog (FITS table).
    """
    def __init__(self, catmembers):
        hdu = fits.open(catmembers)

        # Members catalog
        self.table = hdu[1].data
        self.memrank = self.table['bgg_rank']
        self.ihal_key = 'id_cl'
        self.ihal_cl = self.table[self.ihal_key]
        self.ra = self.table['ra']
        self.dec = self.table['dec']
        self.z = self.table['zpdf']
        self.rvir = self.table['R200_deg_cl']
        self.coords = SkyCoord(self.ra*u.deg, self.dec*u.deg, frame='fk5')


        # Masked catalog containing only BGGs
        # self.mask_bgg = self.memrank == 0 # Select only the brightest
        self.mask_bgg = self.mask_bgg(max_sep=30.) # max_sep in arcsec.
        self.table_bgg = self.table[self.mask_bgg]
        self.ra_bgg = self.table_bgg['ra']
        self.dec_bgg = self.table_bgg['dec']
        self.ihal_bgg = self.table_bgg[self.ihal_key]
        self.z_bgg = self.table_bgg['zpdf']

        self.coords_bgg = SkyCoord(self.ra_bgg*u.deg, self.dec_bgg*u.deg,
                                   frame='fk5')

        hdu.close()

    def mask_bgg(self, max_sep=15.):
        """Select the BGG for each cluster, among the members close enough to
        the cluster centers.

        Parameters
        ----------
        max_sep : float
            Maximum distance from the cluster center, in [arcsec].

        """

        len_table = len(self.table)

        ra_cl = self.table['ra_cl']
        dec_cl = self.table['dec_cl']
        coords_cl = SkyCoord(ra_cl*u.deg, dec_cl*u.deg,
                             frame='fk5')
        sep = self.coords.separation(coords_cl).arcsec

        # Select only members close enough to the X-ray center.
        mask_sep = sep < max_sep

        # Initialize final mask: start with no BGG selected.
        mask_bgg = np.array([False]*len_table)

        for i, ihal in enumerate(set(self.ihal_cl)):
            # Cluster members
            mask_cl = (self.ihal_cl == ihal)

            memrank_cl_sep = self.memrank[mask_cl*mask_sep]
            if len(memrank_cl_sep) > 0:
                mask_bgg = self._add_to_mask_bgg(mask_bgg, mask_sep,
                                                 mask_cl,
                                                 memrank_cl_sep)

        return mask_bgg

    def _add_to_mask_bgg(self, mask_bgg, mask_sep, mask_cl,
                         memrank_cl_sep):
        """Set to True those mask_bgg elements corresponding to BGGs."""
        # The BGG corresponds to the lower rank.
        bgg_rank = min(memrank_cl_sep)

        # Match the BGG of the cluster in the whole catalog: it
        # must be in the separation mask, in the cluster mask, and
        # correspond to the lowest rank (most bright).
        mask_cl_bgg = mask_sep * mask_cl * (self.memrank==bgg_rank)

        # By definition, only one BGG (per cluster) must be
        # selected.
        if len(np.where(mask_cl_bgg==True)[0]) > 1:
            raise RuntimeError("More than one BGG has been "
                               "identified in the cluster {}. This "
                               "should not happen.".format(ihal))

        # Update the final mask and set to True its element
        # corresponding to a BGG. Logically, this corresponds to
        # mask_bgg += mask_cl_bgg.
        mask_bgg = np.logical_or(mask_bgg, mask_cl_bgg)

        return mask_bgg

    def submask_bgg(self, ii):
        """Mask further the member catalogs by matching galaxies close enough
        to the BGG, and fainter than the BGG (those who are brighter
        are either contamination, if too far away from the cluster
        center, or belong to a different cluster).

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.

        Return
        ------
        submask : array
            Mask galaxies close enough to the BGG. Array of Boolean
            elements, 'True' if the condition is satisfied.

        """
        sep = self.coords_bgg[ii].separation(self.coords).deg
        mask_sep = sep < self.rvir[ii]/2.

        memrank_bgg = self.memrank[self.mask_bgg]
        mask_rank = self.memrank >= memrank_bgg[ii]

        submask = mask_sep * mask_rank

        return submask


class ClusterCartesian:
    """Define here the clusters FITS table and related useful
    quantities. Table entries must be adapted to the specific
    catalog. Apply the mask in terms of Cartesian (comoving)
    coordinates.

    Parameters
    ----------
    catcluster : str
        Path to clusters catalog (FITS table).
    """

    def __init__(self, catcluster):
        hubble = cosmo.H(0).value / 100.

        hducluster = fits.open(catcluster)

        # Add label '_cl' to each header, to distinguish clusters from
        # galaxies entries.
        hdrcluster = hducluster[1].header
        for key in hdrcluster.keys():
            kval = hducluster[1].header[key]
            if (isinstance(kval, basestring)) and ('TTYPE' in key):
                hdrcluster[key] = kval + "_cl"

        # FITS data table
        self.table = hducluster[1].data

        # Useful quantities.
        l = self.table['l_cl']
        b = self.table['b_cl']
        distance = self.table['r_cl'] # Mpc
        self.rvir = self.table['R200_cl'] * 1.e-3 / hubble # Mpc
        self.ihal_key = 'ihal_cl'
        self.ihal = self.table[self.ihal_key]

        # Since for the mask we have a for loop over the clusters,
        # here we store the values of astropy lists in separately
        # initialized arrays.  Otherwise in the for loop the
        # coordinates are converted at each iteration, slowing down
        # terribly the code.  (Note that this is not necessary for
        # galaxy coordinates, since in that case we manage the lists
        # without using loops.)
        coord = SkyCoord(l=l*u.deg, b=b*u.deg,
                            distance=distance,
                            frame='galactic')
        y1 = coord.cartesian.x
        y2 = coord.cartesian.y
        y3 = coord.cartesian.z

        self.x1 = np.zeros(len(y1))
        self.x2 = np.zeros(len(y1))
        self.x3 = np.zeros(len(y1))

        for i in range(len(y1)):
            self.x1[i] = y1[i]
            self.x2[i] = y2[i]
            self.x3[i] = y3[i]

        hducluster.close()

    @lru_cache()
    def _compute_separation(self, ii, galaxy):
        """Compute comoving separations from the center of the cluster. Since
        this method is likely to be called with the same arguments by
        other ones, a memoization decorator is used.

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.
        galaxy : obj
            Instance of the GalaxyAngular class.

        Return
        ------
        comov_sep : array
            Comoving separation from the cluster center.

        """
        comov_sep = np.sqrt((galaxy.x1-self.x1[ii])**2
                            +(galaxy.x2-self.x2[ii])**2
                            +(galaxy.x3-self.x3[ii])**2)
        return comov_sep

    def get_distance(self, ii, galaxy):
        """Return comoving separations from the center of the cluster.
        """
        return self._compute_separation(ii, galaxy)

    def mask_gal(self, ii, galaxy):
        """Create a mask by matching galaxies close enough to the
        given cluster.

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.
        galaxy : GalaxyAngular
            Instance of the Galaxy class.

        Return
        ------
        mask_gal : array
            Mask for the galaxy catalog. Array of Boolean elements,
            'True' if the galaxy is a cluster member.
        """
        c1 = self._compute_separation(ii, galaxy) < self.rvir[ii]
        c2 = galaxy.rband < 0.
        mask_gal = c1*c2 # condition 1 AND 2

        return mask_gal


class GalaxyCartesian:
    """Define here the galaxy FITS table and related useful
    quantities. Table entries must be adapted to the specific
    catalog. We assume that a mask will be applied in terms of
    Cartesian (comoving) coordinates.

    Parameters
    ----------
    catgal : str
        Path to clusters catalog (FITS table).
    """

    def __init__(self, catgal):
        hdugalaxy = fits.open(catgal)

        # FITS data table
        self.table = hdugalaxy[1].data

        l = self.table['l']
        b = self.table['b']
        distance = self.table['rr'] # Mpc
        self.rband = self.table['r']
        self.mass = self.table['Mstar']

        # Useful quantities.
        self.galid_name = 'isub'
        coord = SkyCoord(l=l*u.deg, b=b*u.deg,
                             distance=distance,
                             frame='galactic')

        self.x1 = coord.cartesian.x
        self.x2 = coord.cartesian.y
        self.x3 = coord.cartesian.z

        hdugalaxy.close()


class MemberCartesian:
    """Define here the members FITS table and related useful
    quantities. Table entries must be adapted to the specific
    catalog. Define also a method to select BGGs. The mask is applied
    based on Cartesian (comoving) coordinates.

    Parameters
    ----------
    catcluster : str
        Path to clusters catalog (FITS table).
    """
    def __init__(self, catmembers):
        hubble = cosmo.H(0).value / 100.

        hdu = fits.open(catmembers)

        self.table = hdu[1].data

        self.memrank = self.table['bgg_rank']
        self.ihal_key = 'ihal_cl'
        self.ihal_cl = self.table[self.ihal_key]
        l = self.table['l']
        b = self.table['b']
        distance = self.table['rr'] # Mpc
        self.rvir = self.table['R200_cl'] * 1.e-3 / hubble # Mpc

        coord = SkyCoord(l=l*u.deg, b=b*u.deg,
                             distance=distance,
                             frame='galactic')
        self.x1 = coord.cartesian.x
        self.x2 = coord.cartesian.y
        self.x3 = coord.cartesian.z

        self.mask_bgg = self.memrank == 0

        # Masked catalog containing only BGGs
        self.table_bgg = self.table[self.mask_bgg]
        l_bgg = self.table_bgg['l']
        b_bgg = self.table_bgg['b']
        self.ihal_bgg = self.table_bgg[self.ihal_key]
        distance_bgg = self.table_bgg['rr'] # Mpc

        # Since for the mask we have a loop over the BGGs, store the
        # Cartesian coordinates values to newly initialized arrays to
        # avoid computing the coordinate transformation at every
        # iteration.
        coord_bgg = SkyCoord(l=l_bgg*u.deg, b=b_bgg*u.deg,
                             distance=distance_bgg,
                             frame='galactic')
        y1_bgg = coord_bgg.cartesian.x
        y2_bgg = coord_bgg.cartesian.y
        y3_bgg = coord_bgg.cartesian.z

        self.x1_bgg = np.zeros(len(y1_bgg))
        self.x2_bgg = np.zeros(len(y1_bgg))
        self.x3_bgg = np.zeros(len(y1_bgg))

        for i in range(len(y1_bgg)):
            self.x1_bgg[i] = y1_bgg[i]
            self.x2_bgg[i] = y2_bgg[i]
            self.x3_bgg[i] = y3_bgg[i]

        hdu.close()

    def submask_bgg(self,ii):
        """Mask further the member catalogs by matching galaxies close
        enough to the BGG.

        Parameters
        ----------
        ii : int
            Index corresponding to a specific cluster.

        Return
        ------
        submask : array
            Mask galaxies close enough to the BGG. Array of Boolean
            elements, 'True' if the condition is satisfied.
        """

        c1 = np.sqrt((self.x1-self.x1_bgg[ii])**2
                     +(self.x2-self.x2_bgg[ii])**2
                     +(self.x3-self.x3_bgg[ii])**2) < self.rvir / 2.
        submask = c1

        return submask
