from setuptools import setup, find_packages
import getmbrs

setup(name='getmbrs',
      version=getmbrs.__version__,
      description='Identify members from FITS catalogs',
      classifiers=[
          "Development Status :: 4 - Beta",
          "Environment :: Console",
          "Intended Audience :: Science/Research",
          "Programming Language :: Python :: 2.7"
      ],
      author='Francesco Montanari',
      author_email='francesco.montanari@helsinki.fi',
      url='https://gitlab.com/montanari/getmbrs',
      license='GPL3+',
      packages=find_packages(),
      entry_points={'console_scripts':
                    ['getmbrs = getmbrs.getmbrs:parse',]},
)
