# getmbrs
Select member galaxies given a cluster and a galaxy FITS catalog.

Copyright 2016, 2017 Francesco Montanari, Ghassem Gozaliasl.

Licensed under a General Public License, version 3 or (at your option)
any later version.

## Installation
Use the `setup.py` script located under the main directory:
```shell
python setup.py build
python setup.py install --user
```

## Usage
Invoke help message from command line:
```shell
getmbrs --help
```

## Developing
While developing, the code can be launched without previous
installation as:
```shell
python -m getmbrs.getmbrs ARGUMENTS
```
(Running `python getmbrs/getmbrs.py ...` will raise `ValueError:
Attempted relative import in non-package`.)
